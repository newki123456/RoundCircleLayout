package com.newki.round_circle_layout.policy

import android.content.Context
import android.graphics.*
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import androidx.core.graphics.ColorUtils


internal class RoundCircleLayoutShaderPolicy(
    view: View, context: Context, attributeSet: AttributeSet?,
    attrs: IntArray,
    attrIndex: IntArray
) : AbsRoundCirclePolicy(view, context, attributeSet, attrs, attrIndex) {

    private var mBitmapShader: BitmapShader? = null
    private lateinit var mPath: Path
    private lateinit var mDrawableRect: RectF
    private lateinit var mBitmapPaint: Paint
    private lateinit var mShaderMatrix: Matrix
    private var mBitmapWidth = 0
    private var mBitmapHeight = 0

    //阴影的属性
    private val mShadowRect: RectF = RectF()
    private val mShadowPath = Path()
    private lateinit var mShadowPaint: Paint

    init {
        initViewData()
    }

    private fun initViewData() {
        mContainer.setWillNotDraw(false)  //运行ViewGroup绘制

        mDrawableRect = RectF()
        mPath = Path()
        mBitmapPaint = Paint()
        mShaderMatrix = Matrix()
        mShadowPaint = Paint()
    }

    private fun setupRect() {
        val rectF = calculateBounds()
        val let: Float = rectF.left + mShadowSize
        val top: Float = rectF.top + mShadowSize
        val right: Float = rectF.right - mShadowSize
        val bottom: Float = rectF.bottom - mShadowSize

        mDrawableRect.set(let, top, right, bottom)

        //阴影的Rect
        val shadowLet: Float
        val shadowTop: Float
        val shadowRight: Float
        val shadowBottom: Float

        if (mShadowOffsetX > 0) {
            shadowLet = let + mShadowOffsetX
            shadowRight = right
        } else {
            shadowLet = let
            shadowRight = right + mShadowOffsetX
        }

        if (mShadowOffsetY > 0) {
            shadowTop = top + mShadowOffsetY
            shadowBottom = bottom
        } else {
            shadowTop = top
            shadowBottom = bottom + mShadowOffsetY
        }

        mShadowRect.set(shadowLet, shadowTop, shadowRight, shadowBottom)
    }

    //阴影的设置与绘制准备
    private fun setupShadow() {
        if (mShadowSize > 0) {

            mShadowPaint.color = Color.TRANSPARENT
            mShadowPaint.style = Paint.Style.STROKE
            mShadowPaint.strokeWidth = (mShadowSize / 4).toFloat()

            // 如果阴影不带透明度，强制给它设置一点透明度
            if (ColorUtils.setAlphaComponent(mShadowColor, 255) == mShadowColor) {
                mShadowColor = ColorUtils.setAlphaComponent(mShadowColor, 254)
            }
            mShadowPaint.color = mShadowColor

            mShadowPaint.maskFilter = BlurMaskFilter(mShadowSize / 1.2f, BlurMaskFilter.Blur.NORMAL)

        } else {
            mShadowPaint.clearShadowLayer()
        }
    }

    //设置画笔和BitmapShader等
    private fun setupBG() {

        if (mRoundBackgroundDrawable != null && mRoundBackgroundBitmap != null) {

            mBitmapWidth = mRoundBackgroundBitmap!!.width
            mBitmapHeight = mRoundBackgroundBitmap!!.height

            mBitmapShader = BitmapShader(mRoundBackgroundBitmap!!, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)

            if (mRoundBackgroundBitmap!!.width != 2) {
                updateShaderMatrix()
            }

            mBitmapPaint.isAntiAlias = true
            mBitmapPaint.shader = mBitmapShader

        }

    }

    override fun beforeDispatchDraw(canvas: Canvas?) {
        canvas?.clipPath(mPath)
    }

    override fun afterDispatchDraw(canvas: Canvas?) {
    }

    override fun onDraw(canvas: Canvas?): Boolean {

        if (isCircleType) {

            if (mShadowSize > 0) {
                //阴影的绘制
                canvas?.drawOval(mShadowRect, mShadowPaint)
            }

            canvas?.drawCircle(
                mDrawableRect.centerX(), mDrawableRect.centerY(),
                Math.min(mDrawableRect.height() / 2.0f, mDrawableRect.width() / 2.0f), mBitmapPaint
            )

        } else {
            //自定义圆角
            if (mTopLeft > 0 || mTopRight > 0 || mBottomLeft > 0 || mBottomRight > 0) {

                if (mShadowSize > 0) {
                    //阴影的绘制
                    mShadowPath.reset()
                    mShadowPath.addRoundRect(
                        mShadowRect, floatArrayOf(mTopLeft, mTopLeft, mTopRight, mTopRight, mBottomRight, mBottomRight, mBottomLeft, mBottomLeft),
                        Path.Direction.CW
                    )
                    canvas?.drawPath(mShadowPath, mShadowPaint)
                }

                //使用单独的圆角背景
                val path = Path()
                path.addRoundRect(
                    mDrawableRect, floatArrayOf(mTopLeft, mTopLeft, mTopRight, mTopRight, mBottomRight, mBottomRight, mBottomLeft, mBottomLeft),
                    Path.Direction.CW
                )
                canvas?.drawPath(path, mBitmapPaint)

            } else {
                if (mShadowSize > 0) {
                    //阴影的绘制
                    canvas?.drawRoundRect(mShadowRect, mRoundRadius, mRoundRadius, mShadowPaint)
                }

                //使用统一的圆角背景
                canvas?.drawRoundRect(mDrawableRect, mRoundRadius, mRoundRadius, mBitmapPaint)

            }
        }

        //是否需要super再绘制
        return true
    }

    private fun updateShaderMatrix() {
        var scale = 1.0f
        var dx = 0f
        var dy = 0f

        mShaderMatrix.set(null)

        if (mBitmapWidth * mDrawableRect.height() > mDrawableRect.width() * mBitmapHeight) {
            scale = mDrawableRect.height() / mBitmapHeight.toFloat()
            dx = (mDrawableRect.width() - mBitmapWidth * scale) * 0.5f
        } else {
            scale = mDrawableRect.width() / mBitmapWidth.toFloat()
            dy = (mDrawableRect.height() - mBitmapHeight * scale) * 0.5f
        }

        mShaderMatrix.setScale(scale, scale)

        if (isBGCenterCrop) {
            mShaderMatrix.postTranslate((dx + 0.5f).toInt() + mDrawableRect.left, (dy + 0.5f).toInt() + mDrawableRect.top)
        }

        mBitmapShader?.let {
            it.setLocalMatrix(mShaderMatrix)
        }
    }

    override fun onLayout(left: Int, top: Int, right: Int, bottom: Int) {

        setupRect()

        setupBG()

        setupShadow()

        setupRoundPath()
    }


    //裁剪的路径
    private fun setupRoundPath() {
        mPath.reset()

        if (isCircleType) {

            mPath.addOval(calculateBounds(), Path.Direction.CCW)

        } else {

            //如果是圆角-裁剪圆角
            if (mTopLeft > 0 || mTopRight > 0 || mBottomLeft > 0 || mBottomRight > 0) {

                mPath.addRoundRect(
                    calculateBounds(),
                    floatArrayOf(mTopLeft, mTopLeft, mTopRight, mTopRight, mBottomRight, mBottomRight, mBottomLeft, mBottomLeft),
                    Path.Direction.CCW
                )

            } else {

                mPath.addRoundRect(calculateBounds(), mRoundRadius, mRoundRadius, Path.Direction.CCW)
            }

        }

    }

    //手动设置背景的设置
    override fun setBackground(background: Drawable?) {
        setRoundBackgroundDrawable(background)
    }

    override fun setBackgroundColor(color: Int) {
        val drawable = ColorDrawable(color)
        setRoundBackgroundDrawable(drawable)
    }

    override fun setBackgroundResource(resid: Int) {
        val drawable: Drawable = mContainer.context.resources.getDrawable(resid)
        setRoundBackgroundDrawable(drawable)
    }

    override fun setBackgroundDrawable(background: Drawable?) {
        setRoundBackgroundDrawable(background)
    }

    //重新设置Drawable
    private fun setRoundBackgroundDrawable(drawable: Drawable?) {
        mRoundBackgroundDrawable = drawable
        mRoundBackgroundBitmap = getBitmapFromDrawable(mRoundBackgroundDrawable)

        setupBG()

        //重绘
        mContainer.invalidate()
    }
}