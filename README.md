## RoundCircleLayout

可以支持圆角与圆形裁剪的RoundCircleLayout。基于Outline与Shader实现，无需配置硬件加速，无兼容性问题。

1.0.1版本加入阴影的效果

依赖方法：
```
implementation "com.gitee.newki123456:round_circle_layout:1.0.1"
```

#### 如何使用：

自定义属性如下：
```xml
  <!-- 是否绘制圆形 -->
<attr name="is_circle" format="boolean" />
    <!-- 绘制相同的圆角角度 -->
<attr name="round_radius" format="dimension" />
    <!-- 绘制不同的圆角-左上角度 -->
<attr name="topLeft" format="dimension" />
    <!-- 绘制不同的圆角-右上角度 -->
<attr name="topRight" format="dimension" />
    <!-- 绘制不同的圆角-左下角度 -->
<attr name="bottomLeft" format="dimension" />
    <!-- 绘制不同的圆角-右下角度 -->
<attr name="bottomRight" format="dimension" />
    <!-- 绘制背景的颜色 -->
<attr name="round_circle_background_color" format="color" />
    <!-- 绘制背景的图片 -->
<attr name="round_circle_background_drawable" format="reference" />
    <!-- 绘制背景是否居中裁剪 -->
<attr name="is_bg_center_crop" format="boolean" />
    <!-- 阴影大小 -->
<attr name="round_circle_shadowSize" format="dimension" />
    <!-- 阴影颜色 -->
<attr name="round_circle_shadowColor" format="color" />
    <!-- 阴影水平偏移 -->
<attr name="round_circle_shadowOffsetX" format="dimension" />
    <!-- 阴影垂直偏移 -->
<attr name="round_circle_shadowOffsetY" format="dimension" />
```

属实是见名只意了。

在xml中使用如下：
```xml
          <com.newki.round_circle_layout.RoundCircleConstraintLayout
                android:id="@+id/layout_2"
                android:layout_width="@dimen/d_150dp"
                android:layout_height="@dimen/d_150dp"
                android:layout_marginTop="@dimen/d_10dp"
                app:is_circle="true"
                app:round_circle_background_color="#ff00ff"
                app:round_radius="@dimen/d_40dp"/>


          <com.newki.round_circle_layout.RoundCircleLinearLayout
                android:id="@+id/layout_3"
                android:layout_width="@dimen/d_150dp"
                android:layout_height="@dimen/d_150dp"
                android:layout_marginTop="@dimen/d_10dp"
                app:bottomRight="@dimen/d_20dp"
                app:round_circle_background_drawable="@drawable/chengxiao"
                app:topLeft="@dimen/d_20dp"
                app:topRight="@dimen/d_40dp"/>

           <com.newki.round_circle_layout.RoundCircleConstraintLayout
                android:layout_width="@dimen/d_150dp"
                android:layout_height="@dimen/d_150dp"
                android:layout_marginTop="@dimen/d_10dp"
                app:is_bg_center_crop="false"
                app:is_circle="true"
                app:round_circle_background_drawable="@drawable/chengxiao">
```

效果如下：

![](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/434639cbc3514107a103866308d88232~tplv-k3u1fbpfcp-zoom-1.image)


同时也支持代码中设置：

```kotlin
      val layout_3 = findViewById<RoundCircleLinearLayout>(R.id.layout_3)

        findViewById<ViewGroup>(R.id.layout_2).setOnClickListener {

            val drawable = resources.getDrawable(R.drawable.chengxiao)

            it.background = drawable

            layout_3.background = drawable
        }
```


效果：

![](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/8b037e40c18a48b9bf70a8617ca857ae~tplv-k3u1fbpfcp-zoom-1.image)


阴影的效果：

![](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/e579ed805c534c5f840b566c95980907~tplv-k3u1fbpfcp-zoom-1.image)

具体的实现可以参考我的博客：

https://juejin.cn/post/7145267096577343502